# About

This blog is a single file, statically rendered HTML page. The HTML &amp; CSS were generated with a custom version of [PHPetite][1].

You probably want the [*original* version][2] by Bradley Taunt instead, but please don't bug him about issues related to my version and vice versa.

If you *do* use my version, bear in mind that you do so *at your own risk*. If you ask for help I will probably quote *GoodFellas*, because Free Software != Free Support.

*[HTML]: Hyper Text Markup Language
*[CSS]: Cascading Style Sheets
*[GoodFellas]: "Fuck You, Pay Me"

[1]: https://codeberg.org/starbreaker/phpetite "my custom version of PHPetite"
[2]: https://github.com/bradleytaunt/phpetite "the original PHPetite you should use instead"

